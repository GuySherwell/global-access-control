package za.co.sherwell.SpringSecurityApplication;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import models.AuthenticationRequest;
import models.AuthenticationResponse;
import util.JwtUtil;
import za.co.sherwell.SpringSecurityApplication.services.MyUserDetailsService;

@Controller
public class HelloResource {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private MyUserDetailsService userDetailsService;
	
	private JwtUtil jwtTokenUtil = new JwtUtil();
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authentictionRequest) throws Exception {
		System.out.println(authentictionRequest.getUsername());
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authentictionRequest.getUsername(), authentictionRequest.getPassword())
					
			);
		}catch(BadCredentialsException e){
			throw new Exception("Incorrect Username or Password", e);
		}
		
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authentictionRequest.getUsername());
		
		final String jwt = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}
	
	@RequestMapping(value = "/authenticate1", method = RequestMethod.POST)
	@ResponseBody
	public String createAuthenticationToken1(@RequestBody AuthenticationRequest authentictionRequest) throws Exception {
		System.out.println(authentictionRequest.getUsername());
		String jwt;
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authentictionRequest.getUsername(), authentictionRequest.getPassword())
					
			);
		}catch(BadCredentialsException e){
			throw new Exception("Incorrect Username or Password", e);
		}
		
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authentictionRequest.getUsername());
		
		return jwt = jwtTokenUtil.generateToken(userDetails);
	}
	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public ModelAndView mainPage() {
		return new ModelAndView("main");
	}
	
	@RequestMapping(value = "/landing", method = RequestMethod.GET)
	public ModelAndView landingPage() {
		return new ModelAndView("landing");
	}
	
	@RequestMapping(value = "/getUserData", method = RequestMethod.POST)
	@ResponseBody
	public String getUserData() {
		String noData = "user data not found.";
		System.out.println("returning: " + noData);
		return noData;
	}
	
	@RequestMapping(value = "/validateRequest", method = RequestMethod.POST)
	@ResponseBody
	public String validateRequest(HttpServletRequest request) {
		final String autherizationHeader = request.getHeader("Authorization");
		String username = null;
		String jwt = null;
		if(autherizationHeader != null && autherizationHeader.startsWith("Bearer ")) {
			jwt = autherizationHeader.substring(7);
			username = jwtTokenUtil.extractUsername(jwt);
		}
		return username;
	}
}
