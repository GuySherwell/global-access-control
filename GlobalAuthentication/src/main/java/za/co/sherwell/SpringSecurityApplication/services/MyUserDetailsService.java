package za.co.sherwell.SpringSecurityApplication.services;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService{
 
	/*
	 *Overrides Spring UserDetails
	 *Query DB here by username and pass back result to calling class for validation.=
	 */
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return new User("guysherwell@gmail.com", "pass", new ArrayList<>());
	}
}
