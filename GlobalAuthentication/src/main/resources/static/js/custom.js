$(window).ready(function(){

});

$('#auth_user').click(function(){
	var username = $('#username').val();
	var password = $('#password').val();
	$.ajax({
			url: "/authenticate",
			dataType: 'json',
		    type: 'post',
		    contentType: 'application/json',
		    data: JSON.stringify( { "username": username, "password": password } ),
		    processData: false,
		    success: function(data){
		    	document.cookie = "token=" + data.jwt;
		    	location.href = "/landing";
		    },
		    error: function(){
		    	alert("Failed Login");
		    }
	});
});

function getLandingPage(){
	var token = document.cookie;
	token = token.split("=");
	token = token[1];
	$.ajax({
	    url: '/landing',
	    dataType : 'jsonp',
	    type: 'post',
	    beforeSend : function(request) {
	      // set header if JWT is set
	      if (document.cookie) {
	    	  request.setRequestHeader("Authorization", "Bearer " + token);
	      }
	    },
	    success: function(data) {
	      alert("Done")
	    }
	});
} 

