/**
 * 
 */

$(window).ready(function(){
	getLandingPage();
});

function getLandingPage(){
	var token = document.cookie;
	token = token.split("=");
	token = token[1];
	
	var data = $.ajax({
	    url: '/getUserData',
	    type: 'post',
	    beforeSend : function(request) {
	      // set header if JWT is set
	      if (document.cookie) {
	    	  request.setRequestHeader("Authorization", "Bearer " + token);
	      }
	    },
	    success: function(data){
	    	console.log("success")
	    	console.log(data)
	        $('#response').html(data)
	    },
	    error: function(data){
	    	console.log("error");
	    	location.href = "/main";
	    },
	});
}